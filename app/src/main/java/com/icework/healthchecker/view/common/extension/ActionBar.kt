package com.icework.healthchecker.view.common.extension

import androidx.appcompat.app.ActionBar
import com.icework.healthchecker.R

fun ActionBar.configOnlyBackButton(backDrawableRes: Int? = R.drawable.ic_24dp_back_black) {
    setHomeButtonEnabled(true)
    setDisplayHomeAsUpEnabled(true)
    setDisplayShowTitleEnabled(false)
    backDrawableRes?.let(this::setHomeAsUpIndicator)
}

fun ActionBar.configDrawerButton(drawerDrawableRes: Int? = null) {
    setDisplayHomeAsUpEnabled(true)
    drawerDrawableRes?.let(this::setHomeAsUpIndicator)
    setDisplayShowTitleEnabled(false)
}

fun ActionBar.configWithTitle(title: String, backDrawableRes: Int? = null) {
    setHomeButtonEnabled(true)
    setDisplayHomeAsUpEnabled(true)
    setDisplayShowTitleEnabled(true)
    setTitle(title)
    backDrawableRes?.let(this::setHomeAsUpIndicator)
}

fun ActionBar.configOnlyTitle(title: String) {
    setHomeButtonEnabled(false)
    setDisplayHomeAsUpEnabled(false)
    setDisplayShowTitleEnabled(true)
    setTitle(title)
}
