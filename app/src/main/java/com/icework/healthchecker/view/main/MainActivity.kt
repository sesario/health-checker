package com.icework.healthchecker.view.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.icework.healthchecker.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}
