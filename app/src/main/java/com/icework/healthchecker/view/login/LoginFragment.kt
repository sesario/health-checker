package com.icework.healthchecker.view.login

import android.content.Intent
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.icework.healthchecker.R
import com.icework.healthchecker.domain.user.entity.LoginMethod
import com.icework.healthchecker.domain.user.entity.User
import com.icework.healthchecker.view.common.RandomInteger
import com.icework.healthchecker.view.common.Resource
import com.icework.healthchecker.view.common.Status
import com.icework.healthchecker.view.common.base.BaseSimpleFragment
import com.icework.healthchecker.view.common.extension.configOnlyTitle
import com.icework.healthchecker.view.common.extension.handleGenericError
import com.icework.healthchecker.view.common.extension.observeNonNull
import kotlinx.android.synthetic.main.login_fragment.*

class LoginFragment : BaseSimpleFragment<LoginViewModel>() {

    companion object {
        private val RC_SIGN_IN = RandomInteger.next()
    }

    private val gso by lazy {
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
    }

    private val googleSignInClient by lazy { GoogleSignIn.getClient(requireContext(), gso) }

    override fun buildViewModel(): LoginViewModel {
        val factory = LoginViewModelFactory()
        return ViewModelProvider(this, factory)[LoginViewModel::class.java]
    }

    override fun getContentResource(): Int = R.layout.login_fragment

    override fun configureActionBar(actionBar: ActionBar) =
        actionBar.configOnlyTitle(getString(R.string.login_page_title))

    override fun setupNavigationUI(toolbar: Toolbar) = Unit

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_SIGN_IN -> handleSignInResult(GoogleSignIn.getSignedInAccountFromIntent(data))
        }
    }

    override fun initViews() {
        super.initViews()
        btnGoogleLogin.setOnClickListener { onLoginWithGoogle() }
    }

    override fun initLiveDataObservers() {
        super.initLiveDataObservers()
        viewModel.loginLiveData.observeNonNull(viewLifecycleOwner, this::handleLogin)
    }

    private fun handleLogin(resource: Resource<User>) {
        when (resource.status) {
            Status.LOADING -> showLoadingDialog()
            Status.SUCCESS -> handleLoginSuccess()
            Status.ERROR -> resource.throwable?.let(this::handleLoginError)
        }
    }

    private fun handleLoginSuccess() {
        hideLoadingDialog()
        findNavController().popBackStack()
    }

    private fun handleLoginError(throwable: Throwable) {
        hideLoadingDialog()
        handleGenericError(throwable)
    }

    private fun onLoginWithGoogle() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        runCatching {
            val account = completedTask.getResult(ApiException::class.java)
            account?.let(this::onLoginGoogle)
        }.exceptionOrNull()?.let(this::handleGenericError)
    }

    private fun onLoginGoogle(googleSignInAccount: GoogleSignInAccount) {
        val token = googleSignInAccount.idToken ?: return
        val loginMethod = LoginMethod.GoogleLoginMethod(token)
        viewModel.requestLogin(loginMethod)
    }
}