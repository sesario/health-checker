package com.icework.healthchecker.view.common.module

import androidx.fragment.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    fun provideFirebaseAnalytics(): FirebaseAnalytics {
        return FirebaseAnalytics.getInstance(fragment.requireContext())
    }
}
