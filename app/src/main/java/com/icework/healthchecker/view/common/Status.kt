package com.icework.healthchecker.view.common

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
