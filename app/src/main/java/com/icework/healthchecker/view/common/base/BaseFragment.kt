package com.icework.healthchecker.view.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.activity.OnBackPressedCallback
import androidx.annotation.CallSuper
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.icework.healthchecker.R
import com.icework.healthchecker.view.common.dialog.LoadingDialogFragment
import com.icework.healthchecker.view.common.module.DaggerFragmentComponent
import com.icework.healthchecker.view.common.module.FragmentComponent
import com.icework.healthchecker.view.common.module.FragmentModule
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.base_fragment.*
import kotlinx.android.synthetic.main.base_fragment.view.*
import kotlinx.android.synthetic.main.default_toolbar.*

abstract class BaseFragment : Fragment() {

    companion object {
        private const val DIALOG_LOADING = "DIALOG_LOADING"
    }

    protected val component: FragmentComponent by lazy {
        DaggerFragmentComponent.builder().fragmentModule(
            FragmentModule(this)
        ).build()
    }

    private val compositeDisposable = CompositeDisposable()

    private val dialogFragmentMap = mutableMapOf<String, DialogFragment>()

    private var onBackPressedCallback: OnBackPressedCallback? = null

    protected abstract fun getContentResource(): Int?

    protected open fun getActionToolbar(): Toolbar? = toolbar

    protected open fun configureActionBar(actionBar: ActionBar) = Unit

    protected open fun isActionBarShown() = true

    protected open fun isFullScreen() = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(getTopRootLayoutRes(), container, false)
        getContentResource()?.let {
            val contentView = inflater.inflate(it, container, false)
            root.fragmentContent.removeAllViews()
            root.fragmentContent.addView(contentView)
        }
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        takeIf { isActionBarShown() }?.run { getActionToolbar() }?.run {
            (activity as? AppCompatActivity)?.setSupportActionBar(this)
            setupNavigationUI(this)
            (activity as? AppCompatActivity)?.supportActionBar?.let(this@BaseFragment::configureActionBar)
        }

        initViews()
    }

    protected open fun getTopRootLayoutRes() = R.layout.base_fragment

    override fun onDestroyView() {
        super.onDestroyView()
        onBackPressedCallback?.remove()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    protected open fun setupNavigationUI(toolbar: Toolbar) =
        NavigationUI.setupWithNavController(toolbar, findNavController())

    @CallSuper
    protected open fun initViews() {
        if (isFullScreen()) activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        else activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        toolbarContainer?.isVisible = isActionBarShown()
        onBackPressedCallback?.remove()
        onBackPressedCallback = object : OnBackPressedCallback(isOnBackPressedEnabled()) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        onBackPressedCallback?.let {
            activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, it)
        }
    }

    protected fun showCustomDialog(key: String, dialogFragment: DialogFragment) {
        childFragmentManager.let {
            dialogFragmentMap[key]?.dismiss()
            dialogFragmentMap[key] = dialogFragment
            dialogFragmentMap[key]?.show(it, key)
        }
    }

    protected fun hideCustomDialog(key: String) {
        dialogFragmentMap[key]?.dismiss()
    }

    protected fun showLoadingDialog() {
        showLoadingDialog(getString(R.string.general_loading_message))
    }

    protected fun showLoadingDialog(message: String) {
        showCustomDialog(DIALOG_LOADING, LoadingDialogFragment.newInstance(message))
    }

    protected fun hideLoadingDialog() = hideCustomDialog(DIALOG_LOADING)

    protected open fun isOnBackPressedEnabled() = true

    protected open fun onBackPressed() = findNavController().popBackStack()

    protected fun Disposable.collect() = compositeDisposable.add(this)
}
