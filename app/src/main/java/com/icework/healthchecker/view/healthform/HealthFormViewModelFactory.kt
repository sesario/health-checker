package com.icework.healthchecker.view.healthform

import androidx.lifecycle.ViewModel
import com.icework.healthchecker.domain.health.usecase.SubmitHealthUseCase
import com.icework.healthchecker.domain.user.usecase.IsUserLoggedInUseCase
import com.icework.healthchecker.domain.user.usecase.LogoutUseCase
import com.icework.healthchecker.view.common.base.BaseViewModelFactory
import javax.inject.Inject

class HealthFormViewModelFactory : BaseViewModelFactory() {

    @Inject
    lateinit var isUserLoggedInUseCase: IsUserLoggedInUseCase

    @Inject
    lateinit var logoutUseCase: LogoutUseCase

    @Inject
    lateinit var submitHealthUseCase: SubmitHealthUseCase

    init {
        component.inject(this)
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.cast(
            HealthFormViewModel(
                isUserLoggedInUseCase,
                logoutUseCase,
                submitHealthUseCase
            )
        ) as T
    }
}