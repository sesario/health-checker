package com.icework.healthchecker.view.common.module

import com.icework.healthchecker.view.healthform.HealthFormFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [FragmentModule::class])
interface FragmentComponent {

    fun inject(healthFormFragment: HealthFormFragment)
}
