package com.icework.healthchecker.view.common.extension

import com.google.firebase.analytics.FirebaseAnalytics
import com.icework.healthchecker.view.common.entity.BaseAnalyticsEvent

fun FirebaseAnalytics.log(analyticsEvent: BaseAnalyticsEvent) {
    logEvent(analyticsEvent.eventName, analyticsEvent.buildBundle())
}