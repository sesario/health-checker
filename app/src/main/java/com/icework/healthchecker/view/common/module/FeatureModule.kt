package com.icework.healthchecker.view.common.module

import com.icework.healthchecker.data.health.HealthModule
import com.icework.healthchecker.data.user.UserModule
import dagger.Module

@Module(
    includes = [
        UserModule::class,
        HealthModule::class
    ]
)
class FeatureModule
