package com.icework.healthchecker.view.login

import androidx.lifecycle.ViewModel
import com.icework.healthchecker.domain.user.usecase.GetMyUserUseCase
import com.icework.healthchecker.domain.user.usecase.LoginUseCase
import com.icework.healthchecker.view.common.base.BaseViewModelFactory
import javax.inject.Inject

class LoginViewModelFactory : BaseViewModelFactory() {

    @Inject
    lateinit var loginUseCase: LoginUseCase

    @Inject
    lateinit var getMyUserUseCase: GetMyUserUseCase

    init {
        component.inject(this)
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.cast(LoginViewModel(loginUseCase, getMyUserUseCase)) as T
    }
}