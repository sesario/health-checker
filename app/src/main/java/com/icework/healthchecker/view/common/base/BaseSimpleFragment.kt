package com.icework.healthchecker.view.common.base

import android.view.View
import androidx.annotation.CallSuper
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.icework.healthchecker.view.common.Resource
import com.icework.healthchecker.view.common.Status
import com.icework.healthchecker.R
import com.icework.healthchecker.view.common.exceptions.EmptyListException
import com.icework.healthchecker.view.common.exceptions.NoSuchItemException
import com.icework.healthchecker.view.common.extension.loadImage
import io.reactivex.exceptions.CompositeException
import kotlinx.android.synthetic.main.error_page_layout.view.*
import java.net.ConnectException
import java.net.UnknownHostException

abstract class BaseSimpleFragment<T : BaseViewModel> : BaseFragment() {

    protected val viewModel by lazy { buildViewModel() }

    protected open fun findPageLoadingView(): View? = null

    protected open fun findPageContentView(): View? = null

    protected open fun findPageErrorView(): View? = null

    protected open fun findPageEmptyView(): View? = null

    protected abstract fun buildViewModel(): T

    override fun onResume() {
        super.onResume()
        viewModel.loadPage()
    }

    override fun initViews() {
        super.initViews()
        handlePageLoading()
        initLiveDataObservers()
    }

    @CallSuper
    protected open fun initLiveDataObservers() {
        viewModel.pageLoadEvent.observe(viewLifecycleOwner, Observer { it?.let(this::handlePageLoadEvent) })
    }

    @CallSuper
    protected open fun destroyLiveDataObservers() = Unit

    private fun handlePageLoadEvent(resource: Resource<Unit>) {
        when (resource.status) {
            Status.SUCCESS -> handlePageSuccess(findPageContentView())
            Status.LOADING -> handlePageLoading()
            Status.ERROR -> resource.throwable?.let { handlePageError(findPageErrorView(), it) }
        }
    }

    @CallSuper
    protected open fun handlePageSuccess(pageContentView: View?) {
        findPageLoadingView()?.isVisible = false
        pageContentView?.isVisible = true
        findPageErrorView()?.isVisible = false
        findPageEmptyView()?.isVisible = false
    }

    protected open fun handlePageLoading() {
        findPageLoadingView()?.isVisible = true
        findPageContentView()?.isVisible = false
        findPageErrorView()?.isVisible = false
        findPageEmptyView()?.isVisible = false
    }

    @CallSuper
    protected open fun handlePageError(pageErrorView: View?, throwable: Throwable) {
        findPageLoadingView()?.isVisible = false
        findPageContentView()?.isVisible = false
        val isEmpty = when (throwable) {
            is EmptyListException, is NoSuchItemException -> true
            else -> false
        }
        pageErrorView?.isVisible = !isEmpty
        findPageEmptyView()?.isVisible = isEmpty
    }

    protected fun handleGenericPageError(pageErrorView: View?, throwable: Throwable) {
        when (throwable) {
            is ConnectException,
            is UnknownHostException -> pageErrorView?.let { handleNoConnectionPageError(it) }
            is CompositeException -> pageErrorView?.let { handleCompositePageError(it, throwable) }
            is EmptyListException, is NoSuchItemException ->
                findPageEmptyView()?.let { handleEmptyPage(it, throwable) }
            else -> pageErrorView?.let { handleUnknownPageError(it, throwable) }
        }
    }

    private fun handleNoConnectionPageError(pageErrorView: View) {
        pageErrorView.txtErrorPageMessage.text =
            getString(R.string.general_no_internet_error_message)
        pageErrorView.btnErrorPageRetry.text = getString(R.string.general_retry)
        pageErrorView.btnErrorPageRetry.setOnClickListener { viewModel.reloadPage() }
        pageErrorView.imgErrorPage.loadImage(R.mipmap.ic_launcher)
    }

    private fun handleUnknownPageError(pageErrorView: View, throwable: Throwable) {
        pageErrorView.txtErrorPageMessage.text =
            throwable.localizedMessage?.takeIf { it.isNotBlank() }
                ?: getString(R.string.general_error_message)
        pageErrorView.btnErrorPageRetry.text = getString(R.string.general_retry)
        pageErrorView.btnErrorPageRetry.setOnClickListener { viewModel.reloadPage() }
        pageErrorView.imgErrorPage.loadImage(R.mipmap.ic_launcher)
    }

    private fun handleCompositePageError(pageErrorView: View, throwable: CompositeException) {
        val errorMessage =
            throwable.exceptions.foldIndexed("Multiple exceptions:") { index, message, acc ->
                val currentMessage = acc.localizedMessage?.takeIf { it.isNotBlank() }
                    ?: getString(R.string.general_error_message)
                "$message\n${index + 1}. $currentMessage"
            }
        pageErrorView.txtErrorPageMessage.text = errorMessage
        pageErrorView.btnErrorPageRetry.text = getString(R.string.general_retry)
        pageErrorView.btnErrorPageRetry.setOnClickListener { viewModel.reloadPage() }
        pageErrorView.imgErrorPage.loadImage(R.mipmap.ic_launcher)
    }

    protected open fun handleEmptyPage(pageEmptyView: View, throwable: Throwable) = Unit
}
