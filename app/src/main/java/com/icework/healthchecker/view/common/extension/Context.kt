package com.icework.healthchecker.view.common.extension

import android.content.Context
import android.location.LocationManager
import android.net.ConnectivityManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.icework.healthchecker.R
import kotlinx.android.synthetic.main.simple_snackbar_view.view.*

fun Context.showSimpleSnackbar(
    viewGroup: ViewGroup,
    message: String?,
    callback: (() -> Unit)? = null,
    isSafe: (() -> Boolean)? = null
): Snackbar {
    val inflater = LayoutInflater.from(this)
    val snackView =
        inflater.inflate(R.layout.simple_snackbar_view, viewGroup, false)
    val defaultErrorMessage = getString(R.string.general_error_message)
    snackView.txtMessage.text = message ?: defaultErrorMessage
    return showSnackbar(viewGroup, snackView, message, callback, isSafe)
}

private fun Context.showSnackbar(
    viewGroup: ViewGroup,
    snackbarView: View,
    message: String?,
    callback: (() -> Unit)?,
    isSafe: (() -> Boolean)?
): Snackbar {
    val defaultErrorMessage = getString(R.string.general_error_message)
    val errorMessage = message ?: defaultErrorMessage
    val snackbar =
        Snackbar.make(viewGroup, errorMessage, Snackbar.LENGTH_LONG)
    val layout = snackbar.view as Snackbar.SnackbarLayout
    val textView = layout.findViewById(R.id.snackbar_text) as TextView
    textView.visibility = View.INVISIBLE
    layout.setPadding(0, 0, 0, 0)
    layout.addView(snackbarView, 0)
    snackbar.show()

    callback ?: return snackbar
    snackbar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            if (isSafe?.invoke() == true) {
                super.onDismissed(transientBottomBar, event)
                callback()
            }
        }
    })
    return snackbar
}

@Suppress("DEPRECATION")
fun Context.copyToClipboard(label: String, text: String) {
    val sdkVersion = android.os.Build.VERSION.SDK_INT
    if (sdkVersion < android.os.Build.VERSION_CODES.HONEYCOMB) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as android.text.ClipboardManager
        clipboard.text = text
    } else {
        val clipboard =
            getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
        val clip = android.content.ClipData.newPlainText(label, text)
        clipboard.setPrimaryClip(clip)
    }
}

@Suppress("DEPRECATION")
fun Context.isOnline(): Boolean {
    val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    return manager?.activeNetworkInfo?.isConnected ?: false
}

fun Context.isLocationEnabled(): Boolean {
    val manager = getSystemService(Context.LOCATION_SERVICE) as? LocationManager
    val gpsEnabled = manager?.isProviderEnabled(LocationManager.GPS_PROVIDER) ?: false
    val networkLocationEnabled = manager?.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ?: false
    return gpsEnabled || networkLocationEnabled
}
