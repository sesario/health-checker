package com.icework.healthchecker.view.common.extension

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions

private const val FADE_IN_DURATION_MILLIS = 200

fun ImageView.loadImage(
    any: Any,
    placeholder: Any? = null,
    options: RequestOptions = RequestOptions()
) {
    when (placeholder) {
        is Int -> options.error(placeholder)
        is Drawable -> options.error(placeholder)
    }
    Glide.with(context).load(any)
        .transition(DrawableTransitionOptions.withCrossFade(FADE_IN_DURATION_MILLIS))
        .apply(options)
        .into(this)
}

fun ImageView.loadImageNoCache(
    any: Any,
    placeholder: Any? = null,
    options: RequestOptions = RequestOptions()
) {
    val newOptions = options.apply {
        skipMemoryCache(true)
        diskCacheStrategy(DiskCacheStrategy.NONE)
    }
    loadImage(any, placeholder, newOptions)
}
