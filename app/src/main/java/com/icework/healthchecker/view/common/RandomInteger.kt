package com.icework.healthchecker.view.common

import java.util.concurrent.atomic.AtomicInteger

class RandomInteger private constructor() {

    init {
        throw UnsupportedOperationException("cannot be instantiated")
    }

    companion object {

        private val atomicInteger = AtomicInteger(1000)

        operator fun next(): Int {
            return atomicInteger.incrementAndGet()
        }
    }

}
