package com.icework.healthchecker.view.common.extension

import android.app.Activity
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.icework.healthchecker.R
import io.reactivex.exceptions.CompositeException
import java.net.ConnectException
import java.net.UnknownHostException

fun Fragment.hideKeyboard() {
    val currentFocus = if (this is DialogFragment) dialog?.currentFocus else activity?.currentFocus
    val imm = requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
}

fun Fragment.showKeyboard(editText: EditText) {
    if (editText.isEnabled) {
        editText.requestFocus()
        val imm =
            requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun Fragment.showSimpleSnackbar(message: String?) =
    (view as? ViewGroup)?.let { showSimpleSnackbar(it, message) }

fun Fragment.showSimpleSnackbar(
    viewGroup: ViewGroup,
    message: String?
) = context?.showSimpleSnackbar(viewGroup, message)

fun Fragment.handleGenericError(viewGroup: ViewGroup, throwable: Throwable) {
    val errorMessage = when (throwable) {
        is ConnectException,
        is UnknownHostException -> getString(R.string.general_no_internet_error_message)
        is CompositeException -> throwable.exceptions.foldIndexed("Multiple exceptions:") { index, message, acc ->
            val currentMessage = acc.localizedMessage?.takeIf { it.isNotBlank() }
                ?: getString(R.string.general_error_message)
            "$message\n${index + 1}. $currentMessage"
        }
        else -> throwable.localizedMessage?.takeIf { it.isNotBlank() }
            ?: getString(R.string.general_error_message)
    }
    showSimpleSnackbar(viewGroup, errorMessage)
}

fun Fragment.handleGenericError(throwable: Throwable) {
    (view as? ViewGroup)?.let { handleGenericError(it, throwable) }
}
