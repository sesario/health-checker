package com.icework.healthchecker.view.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.DialogFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseDialogFragment : AppCompatDialogFragment() {

    private val compositeDisposable = CompositeDisposable()

    private val dialogFragmentMap = mutableMapOf<String, DialogFragment>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getContentResource(), container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
    }

    protected abstract fun getContentResource(): Int

    @CallSuper
    protected open fun initViews() = Unit

    protected fun showCustomDialog(key: String, dialogFragment: DialogFragment) {
        childFragmentManager.let {
            dialogFragmentMap[key]?.dismiss()
            dialogFragmentMap[key] = dialogFragment
            dialogFragmentMap[key]?.show(it, key)
        }
    }

    protected fun isCustomDialogShowing(key: String): Boolean {
        return dialogFragmentMap.contains(key)
    }

    protected fun hideCustomDialog(key: String) {
        dialogFragmentMap[key]?.dismiss()
    }

    protected fun Disposable.collect() = compositeDisposable.add(this)

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
