package com.icework.healthchecker.view.common.exceptions

class NoSuchItemException : Exception("No such item")
