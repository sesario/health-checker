package com.icework.healthchecker.view.common.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.icework.healthchecker.view.common.Resource
import com.icework.healthchecker.view.common.Status
import com.icework.healthchecker.view.common.ActionLiveData
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import kotlin.random.Random

abstract class BaseViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    internal val pageLoadEvent = ActionLiveData<Resource<Unit>>()

    private var pageLoadEventSingles = mutableMapOf<Int, Single<Any>>()

    internal var firstLoad = true

    open fun loadPage() = resetPageEventSources {
        if (firstLoad) initPage() else resume()
        refreshPage()
        runPage()
    }

    open fun reloadPage() {
        firstLoad = true
        loadPage()
    }

    internal open fun onFragmentResult(result: Bundle) = Unit

    /*
    * A callback which is called when the page refreshes
    *
    * Invoked every time the page shows up
    * */
    @CallSuper
    protected open fun refreshPage() = Unit

    /*
    * A callback which is called when the page needs to load initial condition
    *
    * Invoked only the first time the page is shown and whenever the page is reloaded
    * */
    @CallSuper
    protected open fun initPage() = Unit

    /*
    * A callback which is called when the page resumes
    *
    * Invoked every time except the first time the page is shown
    * */
    @CallSuper
    protected open fun resume() = Unit

    protected fun resetPageEventSources(callback: () -> Unit) {
        pageLoadEventSingles.clear()
        callback()
    }

    protected fun runPage() {
        pageLoadEvent.postValue(Resource(Status.LOADING))
        if (pageLoadEventSingles.isNotEmpty()) {

            val combinedSingles =
                Observable.fromIterable(pageLoadEventSingles.entries).concatMapSingle { entry ->
                    entry.value.map { entry.key to it }
                        .onErrorReturn { entry.key to onMapPageErrorEvent(entry.key, it) }
                }.toList().map { it.toMap() }
                    .map { onMergePageLoadEvents(it) }
                    .doOnSuccess { map ->
                        map.values.find { it is Throwable }?.let { throw it as Throwable }
                    }

            combinedSingles.subscribe({
                pageLoadEvent.postValue(Resource(Status.SUCCESS))
                firstLoad = false
            }, {
                pageLoadEvent.postValue(Resource(Status.ERROR, it))
                firstLoad = false
            }).collect()
        } else {
            val newValue = pageLoadEvent.value ?: Resource(Status.SUCCESS)
            pageLoadEvent.postValue(newValue)
            firstLoad = false
        }
    }

    protected open fun onMergePageLoadEvents(events: Map<Int, Any>): Map<Int, Any> = events

    protected open fun onMapPageErrorEvent(event: Int, throwable: Throwable): Throwable = throwable

    protected open fun <T> Single<T>.asPageLoadEventSource(eventId: Int = Random.nextInt()): Single<T> {
        val single = BehaviorSubject.create<Any>()
        pageLoadEventSingles[eventId] = single.firstOrError()
        return this.observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { single.onNext(it as Any) }
            .doOnError { single.onError(it) }
    }

    protected fun Disposable.collect() = compositeDisposable.add(this)

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}
