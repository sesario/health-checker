package com.icework.healthchecker.view.common.base

import androidx.lifecycle.ViewModelProvider
import com.icework.healthchecker.view.common.module.DaggerViewModelFactoryComponent

abstract class BaseViewModelFactory : ViewModelProvider.Factory {

    protected val component by lazy { DaggerViewModelFactoryComponent.builder().build() }
}
