package com.icework.healthchecker.view.common.exceptions

class EmptyListException : Exception("List is empty")
