package com.icework.healthchecker.view.common.entity

import android.os.Bundle

abstract class BaseAnalyticsEvent(val eventName: String) {

    abstract fun buildBundle(): Bundle
}