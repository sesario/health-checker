package com.icework.healthchecker.view.login

import com.icework.healthchecker.domain.user.entity.LoginMethod
import com.icework.healthchecker.domain.user.entity.User
import com.icework.healthchecker.domain.user.usecase.GetMyUserUseCase
import com.icework.healthchecker.domain.user.usecase.LoginUseCase
import com.icework.healthchecker.view.common.ActionLiveData
import com.icework.healthchecker.view.common.Resource
import com.icework.healthchecker.view.common.Status
import com.icework.healthchecker.view.common.base.BaseViewModel

class LoginViewModel(
    private val loginUseCase: LoginUseCase,
    private val getMyUserUseCase: GetMyUserUseCase
) : BaseViewModel() {

    internal val loginLiveData = ActionLiveData<Resource<User>>()

    internal fun requestLogin(loginMethod: LoginMethod) {
        loginLiveData.sendAction(Resource(Status.LOADING))
        loginUseCase.execute(LoginUseCase.Params(loginMethod))
            .andThen(getMyUserUseCase.execute())
            .subscribe({
                loginLiveData.sendAction(Resource(Status.SUCCESS, it))
            }, {
                loginLiveData.sendAction(Resource(Status.ERROR, it))
            }).collect()
    }
}