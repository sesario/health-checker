package com.icework.healthchecker.view.common.module

import com.icework.healthchecker.view.healthform.HealthFormViewModelFactory
import com.icework.healthchecker.view.login.LoginViewModelFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [FeatureModule::class])
interface ViewModelFactoryComponent {

    fun inject(target: LoginViewModelFactory)
    fun inject(target: HealthFormViewModelFactory)
}
