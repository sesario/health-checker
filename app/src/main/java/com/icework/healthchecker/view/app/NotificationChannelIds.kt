package com.icework.healthchecker.view.app

object NotificationChannelIds {
    const val MAIN = "main"
    const val IN_APP = "in_app"
}
