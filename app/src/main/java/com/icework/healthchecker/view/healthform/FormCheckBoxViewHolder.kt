package com.icework.healthchecker.view.healthform

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.icework.healthchecker.R
import com.icework.healthchecker.view.healthform.entity.FormCheckBoxModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.form_checkbox_row.view.*

class FormCheckBoxViewHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    companion object {
        fun newInstance(parent: ViewGroup): FormCheckBoxViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.form_checkbox_row, parent, false)
            return FormCheckBoxViewHolder(view)
        }
    }

    fun bind(formCheckBoxModel: FormCheckBoxModel) = containerView.run {
        cbValue.isChecked = formCheckBoxModel.checked
        cbValue.text = formCheckBoxModel.value
        edtExtraValue.isVisible = !formCheckBoxModel.extraHint.isNullOrBlank()
        edtExtraValue.hint = formCheckBoxModel.extraHint
        tag = formCheckBoxModel
    }

    fun getLatestFormCheckBoxModel(): FormCheckBoxModel {
        val formCheckBoxModel = containerView.tag as? FormCheckBoxModel
            ?: throw IllegalStateException("Tag must not be null")
        val checked = containerView.cbValue.isChecked
        val extraValue = containerView.edtExtraValue.text.toString()
        return formCheckBoxModel.copy(checked = checked, extraValue = extraValue)
    }
}