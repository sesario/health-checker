package com.icework.healthchecker.view.common.dialog

import android.os.Bundle
import com.icework.healthchecker.R
import com.icework.healthchecker.view.common.base.BaseDialogFragment
import kotlinx.android.synthetic.main.loading_page_layout.*

class LoadingDialogFragment : BaseDialogFragment() {

    companion object {
        private const val MESSAGE = "MESSAGE"
        fun newInstance(message: String) = LoadingDialogFragment().apply {
            arguments = Bundle().apply {
                putString(MESSAGE, message)
            }
        }
    }

    private val loadingMessage by lazy { arguments?.getString(MESSAGE) }

    override fun getContentResource() = R.layout.loading_dialog_fragment

    override fun initViews() {
        super.initViews()
        isCancelable = false
        txtLoadingPageMessage.text = loadingMessage
    }
}
