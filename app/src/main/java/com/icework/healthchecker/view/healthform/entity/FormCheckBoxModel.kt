package com.icework.healthchecker.view.healthform.entity

data class FormCheckBoxModel(
    val id: Long,
    val checked: Boolean,
    val value: String,
    val extraHint: String?,
    val extraValue: String?
)