package com.icework.healthchecker.view.healthform.entity.analytics

import android.os.Bundle
import androidx.core.os.bundleOf
import com.icework.healthchecker.view.common.entity.BaseAnalyticsEvent

class SubmitHealthAnalyticsModel(
    private val healthLevel: Int,
    private val temperature: Double,
    private val symptoms: List<String>,
    private val comingToOffice: Boolean,
    private val remarks: String
) : BaseAnalyticsEvent(EVENT_NAME) {

    companion object {
        private const val EVENT_NAME = "submit_health"
        private const val PARAM_HEALTH_LEVEL = "health_level"
        private const val PARAM_TEMPERATURE = "temperature"
        private const val PARAM_SYMPTOMS = "symptoms"
        private const val PARAM_COMING_TO_OFFICE = "coming_to_office"
        private const val PARAM_REMARKS = "remarks"
    }

    override fun buildBundle(): Bundle = bundleOf(
        PARAM_HEALTH_LEVEL to healthLevel,
        PARAM_TEMPERATURE to temperature,
        PARAM_SYMPTOMS to symptoms,
        PARAM_COMING_TO_OFFICE to comingToOffice,
        PARAM_REMARKS to remarks
    )
}