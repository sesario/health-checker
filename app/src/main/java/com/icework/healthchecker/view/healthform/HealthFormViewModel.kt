package com.icework.healthchecker.view.healthform

import androidx.lifecycle.MutableLiveData
import com.icework.healthchecker.domain.health.usecase.SubmitHealthUseCase
import com.icework.healthchecker.domain.user.usecase.IsUserLoggedInUseCase
import com.icework.healthchecker.domain.user.usecase.LogoutUseCase
import com.icework.healthchecker.view.common.ActionLiveData
import com.icework.healthchecker.view.common.Resource
import com.icework.healthchecker.view.common.Status
import com.icework.healthchecker.view.common.base.BaseViewModel

class HealthFormViewModel(
    private val isUserLoggedInUseCase: IsUserLoggedInUseCase,
    private val logoutUseCase: LogoutUseCase,
    private val submitHealthUseCase: SubmitHealthUseCase
) : BaseViewModel() {

    internal val isUserLoggedInLiveData = ActionLiveData<Boolean>()

    internal val logoutLiveData = ActionLiveData<Resource<Unit>>()

    internal val submitHealthLiveData = ActionLiveData<Resource<Unit>>()

    override fun refreshPage() {
        super.refreshPage()
        requestIsUserLoggedIn()
    }

    private fun requestIsUserLoggedIn() {
        isUserLoggedInUseCase.execute().asPageLoadEventSource().subscribe({
            isUserLoggedInLiveData.value = it
        }, {
            it.printStackTrace()
        }).collect()
    }

    internal fun requestLogout() {
        logoutLiveData.sendAction(Resource(Status.LOADING))
        logoutUseCase.execute().subscribe({
            logoutLiveData.sendAction(Resource(Status.SUCCESS))
        }, {
            logoutLiveData.sendAction(Resource(Status.ERROR, it))
        }).collect()
    }

    internal fun submitHealth(
        healthLevel: Int,
        temperature: Double,
        symptoms: List<String>,
        comingToOffice: Boolean,
        remarks: String
    ) {
        submitHealthLiveData.sendAction(Resource(Status.LOADING))
        val params =
            SubmitHealthUseCase.Params(healthLevel, temperature, symptoms, comingToOffice, remarks)
        submitHealthUseCase.execute(params).subscribe({
            submitHealthLiveData.sendAction(Resource(Status.SUCCESS))
        }, {
            submitHealthLiveData.sendAction(Resource(Status.ERROR, it))
        }).collect()
    }
}