package com.icework.healthchecker.view.healthform

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.core.view.children
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.icework.healthchecker.R
import com.icework.healthchecker.view.common.Resource
import com.icework.healthchecker.view.common.Status
import com.icework.healthchecker.view.common.base.BaseSimpleFragment
import com.icework.healthchecker.view.common.extension.configOnlyTitle
import com.icework.healthchecker.view.common.extension.log
import com.icework.healthchecker.view.common.extension.observeNonNull
import com.icework.healthchecker.view.common.extension.showSimpleSnackbar
import com.icework.healthchecker.view.healthform.entity.FormCheckBoxModel
import com.icework.healthchecker.view.healthform.entity.analytics.SubmitHealthAnalyticsModel
import kotlinx.android.synthetic.main.form_checkbox_row.view.*
import kotlinx.android.synthetic.main.health_form_fragment.*
import javax.inject.Inject

class HealthFormFragment : BaseSimpleFragment<HealthFormViewModel>() {

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    private val symptomOptions by lazy {
        listOf(
            R.string.health_form_symptoms_coughing,
            R.string.health_form_symptoms_head_ache,
            R.string.health_form_symptoms_runny_nose,
            R.string.health_form_symptoms_other
        ).map { id ->
            val extraHint = takeIf { id == R.string.health_form_symptoms_other }
                ?.run { getString(R.string.health_form_symptoms_other_hint) }
            FormCheckBoxModel(id.toLong(), false, getString(id), extraHint, null)
        }
    }

    private val symptomOptionViewHolders = mutableListOf<FormCheckBoxViewHolder>()

    override fun buildViewModel(): HealthFormViewModel {
        val factory = HealthFormViewModelFactory()
        return ViewModelProvider(this, factory)[HealthFormViewModel::class.java]
    }

    override fun getContentResource(): Int = R.layout.health_form_fragment

    override fun configureActionBar(actionBar: ActionBar) =
        actionBar.configOnlyTitle(getString(R.string.health_form_page_title))

    override fun onBackPressed(): Boolean {
        activity?.finish()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun initViews() {
        super.initViews()
        setHasOptionsMenu(true)

        (1..5).forEach {
            val valueView = layoutInflater.inflate(
                R.layout.feeling_value_row,
                llFeelingValues,
                false
            ) as TextView
            valueView.text = it.toString()
            llFeelingValues.addView(valueView)
        }

        this.symptomOptionViewHolders.clear()
        val symptomOptionViewHolders = symptomOptions.map {
            FormCheckBoxViewHolder.newInstance(llSymptoms).apply { bind(it) }
                .also { llSymptoms.addView(it.containerView) }
        }
        this.symptomOptionViewHolders.addAll(symptomOptionViewHolders)
        btnSubmit.setOnClickListener { onSubmit() }
    }

    private fun onSubmit() {
        val healthLevel = acsbFeeling.progress + 1
        val temperature = edtTemperature.text.toString().toDoubleOrNull() ?: 0.0
        val symptoms = getSelectedSymptoms()
        val comingToOffice = when (rgComingToOffice.checkedRadioButtonId) {
            R.id.rbComingToOfficeYes -> true
            R.id.rbComingToOfficeNo -> false
            else -> return
        }
        val remarks = edtRemarks.text.toString()
        viewModel.submitHealth(healthLevel, temperature, symptoms, comingToOffice, remarks)
        val submitHealthAnalyticsModel = SubmitHealthAnalyticsModel(
            healthLevel,
            temperature,
            symptoms,
            comingToOffice,
            remarks
        )
        firebaseAnalytics.log(submitHealthAnalyticsModel)
    }

    private fun getSelectedSymptoms(): List<String> {
        return symptomOptionViewHolders.map { it.getLatestFormCheckBoxModel() }
            .filter { it.checked }
            .map {
                val value = it.value
                val extraValue = it.extraValue
                "$value $extraValue"
            }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.health_form_menus, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.actionLogout -> viewModel.requestLogout().run { true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun initLiveDataObservers() {
        super.initLiveDataObservers()
        viewModel.isUserLoggedInLiveData.observeNonNull(
            viewLifecycleOwner,
            this::handleIsUserLoggedIn
        )
        viewModel.logoutLiveData.observeNonNull(viewLifecycleOwner, this::handleLogout)
        viewModel.submitHealthLiveData.observeNonNull(viewLifecycleOwner, this::handleSubmitHealth)
    }

    private fun handleLogout(resource: Resource<Unit>) {
        when (resource.status) {
            Status.LOADING -> showLoadingDialog()
            Status.SUCCESS -> handleLogoutSuccess()
            Status.ERROR -> hideLoadingDialog()
        }
    }

    private fun handleLogoutSuccess() {
        hideLoadingDialog()
        viewModel.reloadPage()
    }

    private fun handleSubmitHealth(resource: Resource<Unit>) {
        when (resource.status) {
            Status.LOADING -> showLoadingDialog()
            Status.SUCCESS -> handleSubmitHealthSuccess()
            Status.ERROR -> hideLoadingDialog()
        }
    }

    private fun handleSubmitHealthSuccess() {
        hideLoadingDialog()
        viewModel.reloadPage()
        showSimpleSnackbar(getString(R.string.health_form_submit_success_message))
    }

    private fun handleIsUserLoggedIn(isUserLoggedIn: Boolean) {
        if (!isUserLoggedIn) {
            val direction = HealthFormFragmentDirections.actionHealthFormFragmentToLoginFragment()
            findNavController().navigate(direction)
        }
    }
}