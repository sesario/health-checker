package com.icework.healthchecker.view.common

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
open class Resource<T>(val status: Status): Parcelable {

    @IgnoredOnParcel
    var item: T? = null

    @IgnoredOnParcel
    var throwable: Throwable? = null

    constructor(status: Status, item: T): this(status) {
        this.item = item
    }

    constructor(status: Status, throwable: Throwable): this(status) {
        this.throwable = throwable
    }
}
