package com.icework.healthchecker.data.user.entity

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("id") val id: String?,
    @SerializedName("fullName") val fullName: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("picture") val picture: String?
)
