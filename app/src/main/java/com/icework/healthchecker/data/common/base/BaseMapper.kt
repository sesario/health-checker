package com.icework.healthchecker.data.common.base

abstract class BaseMapper<T, R> {

    fun apply(sourceItem: T): R {
        return map(sourceItem)
    }

    protected abstract fun map(item: T): R
}
