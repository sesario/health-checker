package com.icework.healthchecker.data.user.repository

import com.icework.healthchecker.data.user.mapper.UserMapper
import com.icework.healthchecker.domain.user.UserRepository
import com.icework.healthchecker.domain.user.entity.LoginMethod
import com.icework.healthchecker.domain.user.entity.User
import com.icework.healthchecker.firestore.user.UserFirestoreDataSource
import io.reactivex.Completable
import io.reactivex.Single

class UserDataRepository(
    private val userFirestoreDataSource: UserFirestoreDataSource,
    private val userMapper: UserMapper
) : UserRepository {

    override fun login(loginMethod: LoginMethod): Completable {
        return when (loginMethod) {
            is LoginMethod.DefaultLoginMethod -> Completable.error(IllegalStateException("Login method is not supported yet."))
            is LoginMethod.GoogleLoginMethod ->
                userFirestoreDataSource.loginWithGoogle(loginMethod.token)
        }
    }

    override fun getMyUser(): Single<User> {
        return userFirestoreDataSource.getMyUser().map(userMapper::apply)
    }

    override fun logout(): Completable {
        return Completable.fromCallable {
            userFirestoreDataSource.logout()
            Completable.complete()
        }
    }

    override fun isUserLoggedIn(): Single<Boolean> {
        return Single.fromCallable { userFirestoreDataSource.isLoggedIn() }
    }
}