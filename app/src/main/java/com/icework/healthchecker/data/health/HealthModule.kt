package com.icework.healthchecker.data.health

import com.icework.healthchecker.data.health.repository.HealthDataRepository
import com.icework.healthchecker.domain.health.HealthRepository
import com.icework.healthchecker.firestore.health.HealthFirestoreDataSource
import com.icework.healthchecker.firestore.health.HealthFirestoreModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [HealthFirestoreModule::class])
class HealthModule {

    @Singleton
    @Provides
    fun provideRepository(
        healthFirestoreDataSource: HealthFirestoreDataSource
    ): HealthRepository = HealthDataRepository(healthFirestoreDataSource)
}
