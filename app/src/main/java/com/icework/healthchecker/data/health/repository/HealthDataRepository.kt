package com.icework.healthchecker.data.health.repository

import com.icework.healthchecker.domain.health.HealthRepository
import com.icework.healthchecker.firestore.health.HealthFirestoreDataSource
import io.reactivex.Completable

class HealthDataRepository(
    private val healthFirestoreDataSource: HealthFirestoreDataSource
) : HealthRepository {

    override fun submitHealth(
        healthLevel: Int,
        temperature: Double,
        symptoms: List<String>,
        comingToOffice: Boolean,
        remarks: String
    ): Completable {
        return healthFirestoreDataSource.submitHealth(
            healthLevel,
            temperature,
            symptoms,
            comingToOffice,
            remarks
        )
    }
}