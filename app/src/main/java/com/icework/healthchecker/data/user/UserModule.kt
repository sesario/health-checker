package com.icework.healthchecker.data.user

import com.icework.healthchecker.data.user.mapper.UserMapper
import com.icework.healthchecker.data.user.repository.UserDataRepository
import com.icework.healthchecker.domain.user.UserRepository
import com.icework.healthchecker.firestore.user.UserFirestoreDataSource
import com.icework.healthchecker.firestore.user.UserFirestoreModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [UserFirestoreModule::class])
class UserModule {

    @Singleton
    @Provides
    fun provideRepository(
        userFirestoreDataSource: UserFirestoreDataSource,
        userMapper: UserMapper
    ): UserRepository = UserDataRepository(userFirestoreDataSource, userMapper)
}
