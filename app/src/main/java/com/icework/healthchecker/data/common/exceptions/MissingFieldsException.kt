package com.icework.healthchecker.data.common.exceptions

class MissingFieldsException(missingFields: List<String>) :
    Exception("Some fields are missing: $missingFields")
