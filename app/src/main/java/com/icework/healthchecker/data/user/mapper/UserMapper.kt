package com.icework.healthchecker.data.user.mapper

import com.icework.healthchecker.data.common.base.BaseMapper
import com.icework.healthchecker.data.common.exceptions.MissingFieldsException
import com.icework.healthchecker.data.user.entity.UserResponse
import com.icework.healthchecker.domain.user.entity.User
import javax.inject.Inject

class UserMapper @Inject constructor() : BaseMapper<UserResponse, User>() {

    override fun map(item: UserResponse): User {
        return item.run {
            val missingFields = mutableListOf<String>()
            id ?: missingFields.add("id")
            fullName ?: missingFields.add("fullName")
            email ?: missingFields.add("email")
            if (missingFields.isNotEmpty()) throw MissingFieldsException(missingFields)
            User(
                id!!,
                fullName!!,
                email!!,
                picture.orEmpty()
            )
        }
    }
}
