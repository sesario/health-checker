package com.icework.healthchecker.data.health.mapper

import com.icework.healthchecker.data.common.base.BaseMapper
import com.icework.healthchecker.data.common.exceptions.MissingFieldsException
import com.icework.healthchecker.data.health.entity.HealthResponse
import com.icework.healthchecker.domain.common.utils.DateUtils
import com.icework.healthchecker.domain.health.entity.Health
import javax.inject.Inject

class HealthMapper @Inject constructor() : BaseMapper<HealthResponse, Health>() {

    override fun map(item: HealthResponse): Health {
        return item.run {
            val missingFields = mutableListOf<String>()
            id ?: missingFields.add("id")
            timestamp ?: missingFields.add("timestamp")
            healthLevel ?: missingFields.add("healthLevel")
            comingToOffice ?: missingFields.add("comingToOffice")
            if (missingFields.isNotEmpty()) throw MissingFieldsException(missingFields)
            Health(
                id!!,
                timestamp!!.takeIf { it.isNotBlank() }
                    ?.let { DateUtils.stringToDate(it, DateUtils.TIME_STAMP_FORMAT) },
                healthLevel!!,
                temperature ?: 0.0,
                symptoms.orEmpty(),
                comingToOffice!!,
                remarks.orEmpty()
            )
        }
    }
}
