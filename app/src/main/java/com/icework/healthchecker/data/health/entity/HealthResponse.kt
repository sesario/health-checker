package com.icework.healthchecker.data.health.entity

import com.google.gson.annotations.SerializedName

data class HealthResponse(
    @SerializedName("id") val id: String?,
    @SerializedName("timestamp") val timestamp: String?,
    @SerializedName("healthLevel") val healthLevel: Int?,
    @SerializedName("temperature") val temperature: Double?,
    @SerializedName("symptoms") val symptoms: List<String>?,
    @SerializedName("comingToOffice") val comingToOffice: Boolean?,
    @SerializedName("remarks") val remarks: String?
)
