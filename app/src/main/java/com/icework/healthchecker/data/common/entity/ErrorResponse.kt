package com.icework.healthchecker.data.common.entity

import com.google.gson.annotations.SerializedName

class ErrorResponse(
    @SerializedName("code") val code: Int?,
    @SerializedName("message") val message: String?
)
