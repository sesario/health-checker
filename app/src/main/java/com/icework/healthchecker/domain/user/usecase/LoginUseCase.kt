package com.icework.healthchecker.domain.user.usecase

import com.icework.healthchecker.domain.common.CompletableErrorTransformer
import com.icework.healthchecker.domain.common.base.BaseCompletableUseCase
import com.icework.healthchecker.domain.common.exceptions.IllegalParamsException
import com.icework.healthchecker.domain.user.UserRepository
import com.icework.healthchecker.domain.user.entity.LoginMethod
import io.reactivex.Completable
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val userRepository: UserRepository,
    errorTransformer: CompletableErrorTransformer
) : BaseCompletableUseCase<LoginUseCase.Params>(errorTransformer) {

    override fun buildCompletable(params: Params?): Completable {
        return params?.run {
            userRepository.login(loginMethod)
        } ?: throw IllegalParamsException()
    }

    class Params(
        val loginMethod: LoginMethod
    ) : BaseCompletableUseCase.Params
}
