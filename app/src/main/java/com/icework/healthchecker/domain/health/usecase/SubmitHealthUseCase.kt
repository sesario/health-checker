package com.icework.healthchecker.domain.health.usecase

import com.icework.healthchecker.domain.common.CompletableErrorTransformer
import com.icework.healthchecker.domain.common.base.BaseCompletableUseCase
import com.icework.healthchecker.domain.common.exceptions.IllegalParamsException
import com.icework.healthchecker.domain.health.HealthRepository
import io.reactivex.Completable
import javax.inject.Inject

class SubmitHealthUseCase @Inject constructor(
    private val healthRepository: HealthRepository,
    errorTransformer: CompletableErrorTransformer
) : BaseCompletableUseCase<SubmitHealthUseCase.Params>(errorTransformer) {

    override fun buildCompletable(params: Params?): Completable {
        return params?.run {
            healthRepository.submitHealth(
                healthLevel,
                temperature,
                symptoms,
                comingToOffice,
                remarks
            )
        } ?: throw IllegalParamsException()
    }

    class Params(
        val healthLevel: Int,
        val temperature: Double,
        val symptoms: List<String>,
        val comingToOffice: Boolean,
        val remarks: String
    ) : BaseCompletableUseCase.Params
}
