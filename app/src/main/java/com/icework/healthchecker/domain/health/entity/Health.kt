package com.icework.healthchecker.domain.health.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Health(
    val id: String,
    val timestamp: Date?,
    val healthLevel: Int,
    val temperature: Double,
    val symptoms: List<String>,
    val comingToOffice: Boolean,
    val remarks: String
) : Parcelable
