package com.icework.healthchecker.domain.user.usecase

import com.icework.healthchecker.domain.common.CompletableErrorTransformer
import com.icework.healthchecker.domain.common.base.BaseCompletableUseCase
import com.icework.healthchecker.domain.user.UserRepository
import io.reactivex.Completable
import javax.inject.Inject

class LogoutUseCase @Inject constructor(
    private val userRepository: UserRepository,
    errorTransformer: CompletableErrorTransformer
) : BaseCompletableUseCase<BaseCompletableUseCase.Params>(errorTransformer) {

    override fun buildCompletable(params: Params?): Completable {
        return userRepository.logout()
    }
}
