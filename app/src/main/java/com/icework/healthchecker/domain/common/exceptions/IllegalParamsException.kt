package com.icework.healthchecker.domain.common.exceptions

import java.lang.Exception

class IllegalParamsException : Exception("Illegal Params")
