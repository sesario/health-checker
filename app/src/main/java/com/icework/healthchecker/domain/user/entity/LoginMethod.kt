package com.icework.healthchecker.domain.user.entity

sealed class LoginMethod {
    class DefaultLoginMethod(val email: String, val password: String) : LoginMethod()
    class GoogleLoginMethod(val token: String) : LoginMethod()
}
