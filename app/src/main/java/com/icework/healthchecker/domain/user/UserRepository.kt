package com.icework.healthchecker.domain.user

import com.icework.healthchecker.domain.user.entity.LoginMethod
import com.icework.healthchecker.domain.user.entity.User
import io.reactivex.Completable
import io.reactivex.Single

interface UserRepository {

    fun login(loginMethod: LoginMethod): Completable

    fun getMyUser(): Single<User>

    fun logout(): Completable

    fun isUserLoggedIn(): Single<Boolean>
}
