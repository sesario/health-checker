package com.icework.healthchecker.domain.health

import io.reactivex.Completable

interface HealthRepository {

    fun submitHealth(
        healthLevel: Int,
        temperature: Double,
        symptoms: List<String>,
        comingToOffice: Boolean,
        remarks: String
    ): Completable
}
