package com.icework.healthchecker.domain.common

import com.google.gson.Gson
import javax.inject.Inject

class ErrorHelper @Inject constructor(private val gson: Gson) {

    fun buildThrowable(throwable: Throwable): Throwable {
        return throwable // Map throwable coming from data layer here
    }
}
