package com.icework.healthchecker.domain.common.base

import com.icework.healthchecker.domain.common.SingleErrorTransformer
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class BaseSingleUseCase<T, R : BaseSingleUseCase.Params>(private val errorTransformer: SingleErrorTransformer<T>) {

    interface Params

    abstract fun buildSingle(params: R? = null): Single<T>

    fun execute(params: R? = null): Single<T> {
        return buildSingle(params)
            .compose(errorTransformer)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
