package com.icework.healthchecker.domain.user.usecase

import com.icework.healthchecker.domain.common.SingleErrorTransformer
import com.icework.healthchecker.domain.common.base.BaseSingleUseCase
import com.icework.healthchecker.domain.user.UserRepository
import com.icework.healthchecker.domain.user.entity.User
import io.reactivex.Single
import javax.inject.Inject

class GetMyUserUseCase @Inject constructor(
    private val userRepository: UserRepository,
    errorTransformer: SingleErrorTransformer<User>
) : BaseSingleUseCase<User, BaseSingleUseCase.Params>(errorTransformer) {

    override fun buildSingle(params: Params?): Single<User> {
        return userRepository.getMyUser()
    }
}
