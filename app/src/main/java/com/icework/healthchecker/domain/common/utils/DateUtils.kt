package com.icework.healthchecker.domain.common.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    const val TIME_STAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
    const val DATE_FORMAT = "yyyy-MM-dd"
    const val TIME_FORMAT = "HH:mm"
    const val SHOWN_TIME = "HH:mm"
    const val SHOWN_DATE = "dd MMM yyyy"
    const val SHOWN_DATE_NUMBER = "dd"
    const val SHOWN_MONTH = "MMM yyyy"
    const val SHOWN_DATE_TIME = "HH:mm, dd MMM yyyy"

    fun dateToString(date: Date, format: String): String {
        return buildDateFormat(format).format(date)
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    fun stringToDate(dateString: String, format: String): Date {
        return buildDateFormat(format).parse(dateString)
    }

    private fun buildDateFormat(format: String) = when (format) {
        TIME_STAMP_FORMAT -> buildUtcDateFormat(format)
        else -> buildSimpleDateFormat(format)
    }

    private fun buildSimpleDateFormat(format: String) =
        SimpleDateFormat(format, Locale.getDefault())

    private fun buildUtcDateFormat(format: String) =
        SimpleDateFormat(format, Locale.getDefault()).apply {
            timeZone = TimeZone.getTimeZone("UTC")
        }
}
