package com.icework.healthchecker.domain.common.base

import com.icework.healthchecker.domain.common.CompletableErrorTransformer
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class BaseCompletableUseCase<R : BaseCompletableUseCase.Params>(private val errorTransformer: CompletableErrorTransformer) {

    interface Params

    abstract fun buildCompletable(params: R? = null): Completable

    fun execute(params: R? = null): Completable {
        return buildCompletable(params)
            .compose(errorTransformer)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
