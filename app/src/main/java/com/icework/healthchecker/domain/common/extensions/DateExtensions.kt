package com.icework.healthchecker.domain.common.extensions

import java.util.*
import java.util.concurrent.TimeUnit

fun Date.add(calendarFieldToAdd: Int, dayToAdd: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(calendarFieldToAdd, dayToAdd)
    return Date(calendar.timeInMillis)
}

fun Date.subtract(dateToSubtractBy: Date, resultTimeUnit: TimeUnit): Long {
    return resultTimeUnit.convert(this.time - dateToSubtractBy.time, TimeUnit.MILLISECONDS)
}
