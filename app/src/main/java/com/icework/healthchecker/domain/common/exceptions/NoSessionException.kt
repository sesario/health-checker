package com.icework.healthchecker.domain.common.exceptions

import java.lang.Exception

class NoSessionException : Exception("No valid session")
