package com.icework.healthchecker.domain.common.exceptions

import com.icework.healthchecker.data.common.entity.ErrorResponse

open class ServerException(
    response: ErrorResponse,
    val title: String? = "Server Exception"
) : Exception(response.message) {

    class BadRequestHttpException(response: ErrorResponse) :
        ServerException(response, "Bad Request")

    class UnauthorizedHttpException(response: ErrorResponse) :
        ServerException(response, "Unauthorized")

    class ForbiddenHttpException(response: ErrorResponse) :
        ServerException(response, "Forbidden")

    class UnProcessableEntityHttpException(response: ErrorResponse) :
        ServerException(response, "UnProcessable Entity")

    class InternalServerErrorHttpException(response: ErrorResponse) :
        ServerException(response, "Internal Server Error")

    class NotFoundErrorHttpException(response: ErrorResponse) :
        ServerException(response, "Not Found")
}
