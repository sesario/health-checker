package com.icework.healthchecker.domain.user.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: String,
    val fullName: String,
    val email: String,
    val picture: String
) : Parcelable
