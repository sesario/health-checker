package com.icework.healthchecker.firestore.user.transformer

import com.icework.healthchecker.data.common.base.BaseMapper
import com.icework.healthchecker.data.user.entity.UserResponse
import com.icework.healthchecker.firestore.user.entity.UserFdo

class UserFdoToAccountResponseTransformer(private val picture: String) : BaseMapper<UserFdo, UserResponse>() {

    override fun map(item: UserFdo): UserResponse = item.run {
        UserResponse(id, fullName, email, picture)
    }
}
