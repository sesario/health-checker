package com.icework.healthchecker.firestore.health.entity

import androidx.annotation.Keep
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.PropertyName
import java.util.*

@Keep
@IgnoreExtraProperties
class HealthFdo {

    companion object {
        const val ID = "id"
        const val TIMESTAMP = "timestamp"
        const val HEALTH_LEVEL = "healthLevel"
        const val TEMPERATURE = "temperature"
        const val SYMPTOMS = "symptoms"
        const val COMING_TO_OFFICE = "comingToOffice"
        const val REMARKS = "remarks"
    }

    @PropertyName(ID)
    val id: String? = null

    @PropertyName(TIMESTAMP)
    val timestamp: Date? = null

    @PropertyName(HEALTH_LEVEL)
    val healthLevel: Int? = null

    @PropertyName(TEMPERATURE)
    val temperature: Double? = null

    @PropertyName(SYMPTOMS)
    val symptoms: List<String>? = null

    @PropertyName(COMING_TO_OFFICE)
    val comingToOffice: Boolean? = null

    @PropertyName(REMARKS)
    val remarks: String? = null
}
