package com.icework.healthchecker.firestore.common.exceptions

import com.icework.healthchecker.data.common.entity.ErrorResponse
import com.icework.healthchecker.domain.common.exceptions.ServerException
import com.icework.healthchecker.firestore.common.constants.FirebaseErrorCodes

class MissingEntityException : ServerException(
    ErrorResponse(FirebaseErrorCodes.FIREBASE_MISSING_ENTITY, "Missing data")
)
