package com.icework.healthchecker.firestore.health

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.icework.healthchecker.firestore.common.exceptions.UnauthorizedUserException
import com.icework.healthchecker.firestore.common.extensions.toCompletable
import com.icework.healthchecker.firestore.common.extensions.userHealth
import com.icework.healthchecker.firestore.common.extensions.users
import com.icework.healthchecker.firestore.health.entity.HealthFdo
import com.icework.healthchecker.firestore.user.entity.UserFdo
import io.reactivex.Completable

class HealthFirestoreDataSource(
    private val firebaseAuth: FirebaseAuth,
    private val firestore: FirebaseFirestore
) {

    fun submitHealth(
        healthLevel: Int,
        temperature: Double,
        symptoms: List<String>,
        comingToOffice: Boolean,
        remarks: String
    ): Completable = Completable.defer {
        val firebaseUser = firebaseAuth.currentUser ?: throw UnauthorizedUserException()
        val newUserHealthReference = firestore.userHealth(firebaseUser.uid).document()
        val newUserHealthMap = mapOf(
            HealthFdo.ID to newUserHealthReference.id,
            HealthFdo.TIMESTAMP to FieldValue.serverTimestamp(),
            HealthFdo.HEALTH_LEVEL to healthLevel,
            HealthFdo.TEMPERATURE to temperature,
            HealthFdo.SYMPTOMS to symptoms,
            HealthFdo.COMING_TO_OFFICE to comingToOffice,
            HealthFdo.REMARKS to remarks
        )
        newUserHealthReference.set(newUserHealthMap).toCompletable()
    }
}
