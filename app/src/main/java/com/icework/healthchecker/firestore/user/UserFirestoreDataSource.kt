package com.icework.healthchecker.firestore.user

import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.icework.healthchecker.data.user.entity.UserResponse
import com.icework.healthchecker.firestore.common.exceptions.MissingEntityException
import com.icework.healthchecker.firestore.common.exceptions.MissingEntityPartException
import com.icework.healthchecker.firestore.common.exceptions.UnauthorizedUserException
import com.icework.healthchecker.firestore.common.extensions.toCompletable
import com.icework.healthchecker.firestore.common.extensions.toSingle
import com.icework.healthchecker.firestore.common.extensions.users
import com.icework.healthchecker.firestore.user.entity.UserFdo
import com.icework.healthchecker.firestore.user.transformer.UserFdoToAccountResponseTransformer
import io.reactivex.Completable
import io.reactivex.Single

class UserFirestoreDataSource(
    private val firebaseAuth: FirebaseAuth,
    private val firestore: FirebaseFirestore
) {

    fun loginWithGoogle(token: String): Completable = Completable.defer {
        loginWithCredential(GoogleAuthProvider.getCredential(token, null))
    }

    private fun loginWithCredential(authCredential: AuthCredential): Completable =
        Completable.defer {
            firestore.runTransaction { transaction ->
                val authResult =
                    firebaseAuth.signInWithCredential(authCredential).toSingle().blockingGet()
                val userId = authResult.user?.uid ?: throw MissingEntityPartException()
                val email = authResult.user?.email ?: throw MissingEntityPartException()
                val displayName =
                    authResult.user?.displayName ?: throw MissingEntityPartException()
                val userFdo = firestore.users().whereEqualTo(UserFdo.EMAIL, email).get()
                    .toSingle().blockingGet().firstOrNull()?.toObject(UserFdo::class.java)
                if (userFdo == null) {
                    val newUser = mapOf(
                        UserFdo.ID to userId,
                        UserFdo.EMAIL to email,
                        UserFdo.FULL_NAME to displayName
                    )
                    transaction.set(
                        firestore.users().document(userId),
                        newUser,
                        SetOptions.merge()
                    )
                }
            }.toCompletable()
        }

    fun logout() = firebaseAuth.signOut()

    fun getMyUser(): Single<UserResponse> = Single.defer {
        val firebaseUser = firebaseAuth.currentUser ?: throw UnauthorizedUserException()
        firestore.users().document(firebaseUser.uid).get().toSingle()
            .map { it.toObject(UserFdo::class.java) ?: throw MissingEntityException() }
            .map { userFdo -> UserFdoToAccountResponseTransformer("").apply(userFdo) }
    }

    fun isLoggedIn() = firebaseAuth.currentUser != null
}
