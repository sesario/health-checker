package com.icework.healthchecker.firestore.user

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.icework.healthchecker.firestore.common.module.FirestoreModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [FirestoreModule::class])
class UserFirestoreModule {

    @Singleton
    @Provides
    fun provideFirestoreDataSource(
        firebaseAuth: FirebaseAuth,
        firestore: FirebaseFirestore
    ): UserFirestoreDataSource {
        return UserFirestoreDataSource(firebaseAuth, firestore)
    }
}
