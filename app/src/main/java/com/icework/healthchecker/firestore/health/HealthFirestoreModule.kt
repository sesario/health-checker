package com.icework.healthchecker.firestore.health

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.icework.healthchecker.firestore.common.module.FirestoreModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [FirestoreModule::class])
class HealthFirestoreModule {

    @Singleton
    @Provides
    fun provideFirestoreDataSource(
        firebaseAuth: FirebaseAuth,
        firestore: FirebaseFirestore
    ): HealthFirestoreDataSource {
        return HealthFirestoreDataSource(firebaseAuth, firestore)
    }
}
