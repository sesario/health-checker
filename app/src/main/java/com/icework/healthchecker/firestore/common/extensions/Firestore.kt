package com.icework.healthchecker.firestore.common.extensions

import com.google.firebase.firestore.FirebaseFirestore

fun FirebaseFirestore.users() = collection("users")

fun FirebaseFirestore.userHealth(userId: String) =
    collection("users").document(userId).collection("health")
