package com.icework.healthchecker.firestore.common.module

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.icework.healthchecker.domain.common.module.CommonModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [CommonModule::class])
class FirestoreModule {

    @Singleton
    @Provides
    fun provideFirestoreDatabase() = FirebaseFirestore.getInstance()

    @Singleton
    @Provides
    fun provideFirebaseAuth() = FirebaseAuth.getInstance()
}
