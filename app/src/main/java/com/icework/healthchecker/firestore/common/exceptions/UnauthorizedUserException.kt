package com.icework.healthchecker.firestore.common.exceptions

import com.icework.healthchecker.firestore.common.constants.FirebaseErrorCodes
import com.icework.healthchecker.data.common.entity.ErrorResponse
import com.icework.healthchecker.domain.common.exceptions.ServerException

class UnauthorizedUserException : ServerException(
    ErrorResponse(
        FirebaseErrorCodes.FIREBASE_UNAUTHORIZED_USER,
        "Unauthorized user, please login!"
    )
)
