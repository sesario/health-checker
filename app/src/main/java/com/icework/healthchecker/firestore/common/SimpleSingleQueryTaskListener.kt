package com.icework.healthchecker.firestore.common

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import io.reactivex.subjects.SingleSubject

class SimpleSingleQueryTaskListener<T> : OnCompleteListener<T> {

    val single: SingleSubject<T> = SingleSubject.create()

    override fun onComplete(task: Task<T>) {
        if (task.isSuccessful) {
            task.result?.let(single::onSuccess) ?: single.onError(NullPointerException("Null task result"))
        } else task.exception?.let(single::onError)
            ?: single.onError(UnknownError("Unknown task exception"))
    }
}
