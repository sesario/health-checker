package com.icework.healthchecker.firestore.user.entity

import androidx.annotation.Keep
import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.PropertyName

@Keep
@IgnoreExtraProperties
class UserFdo {

    companion object {
        const val ID = "id"
        const val FULL_NAME = "fullName"
        const val EMAIL = "email"
    }

    @PropertyName(ID)
    val id: String? = null

    @PropertyName(EMAIL)
    val email: String? = null

    @PropertyName(FULL_NAME)
    val fullName: String? = null
}
