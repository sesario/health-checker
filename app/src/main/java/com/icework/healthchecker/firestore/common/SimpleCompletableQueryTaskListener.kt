package com.icework.healthchecker.firestore.common

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import io.reactivex.subjects.CompletableSubject

class SimpleCompletableQueryTaskListener<T> : OnCompleteListener<T> {

    val completable: CompletableSubject = CompletableSubject.create()

    override fun onComplete(task: Task<T>) {
        if (task.isSuccessful) {
            completable.onComplete()
        } else task.exception?.let(completable::onError)
            ?: completable.onError(UnknownError("Unknown task exception"))
    }
}
