package com.icework.healthchecker.firestore.health.transformer

import com.icework.healthchecker.data.common.base.BaseMapper
import com.icework.healthchecker.data.health.entity.HealthResponse
import com.icework.healthchecker.domain.common.utils.DateUtils
import com.icework.healthchecker.firestore.health.entity.HealthFdo

class HealthFdoToHealthResponseTransformer : BaseMapper<HealthFdo, HealthResponse>() {

    override fun map(item: HealthFdo): HealthResponse = item.run {
        HealthResponse(
            id,
            timestamp?.let { DateUtils.dateToString(it, DateUtils.TIME_STAMP_FORMAT) },
            healthLevel,
            temperature,
            symptoms,
            comingToOffice,
            remarks
        )
    }
}
