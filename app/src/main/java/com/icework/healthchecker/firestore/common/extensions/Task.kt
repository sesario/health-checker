package com.icework.healthchecker.firestore.common.extensions

import com.google.android.gms.tasks.Task
import com.icework.healthchecker.firestore.common.SimpleCompletableQueryTaskListener
import com.icework.healthchecker.firestore.common.SimpleSingleQueryTaskListener
import io.reactivex.Completable
import io.reactivex.Single

fun <T> Task<T>.toSingle(): Single<T> {
    val taskListener = SimpleSingleQueryTaskListener<T>()
    addOnCompleteListener(taskListener)
    return taskListener.single
}

fun <T> Task<T>.toCompletable(): Completable {
    val taskListener = SimpleCompletableQueryTaskListener<T>()
    addOnCompleteListener(taskListener)
    return taskListener.completable
}
