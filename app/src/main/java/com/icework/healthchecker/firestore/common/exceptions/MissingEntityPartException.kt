package com.icework.healthchecker.firestore.common.exceptions

import com.icework.healthchecker.data.common.entity.ErrorResponse
import com.icework.healthchecker.domain.common.exceptions.ServerException
import com.icework.healthchecker.firestore.common.constants.FirebaseErrorCodes

class MissingEntityPartException : ServerException(
    ErrorResponse(FirebaseErrorCodes.FIREBASE_MISSING_ENTITY_PART, "Some data are broken")
)
